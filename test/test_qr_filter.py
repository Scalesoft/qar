import unittest
import os

from qar.decode import decode_qr


class QrFilterTests(unittest.TestCase):

    def test_read_noisy_qrcode(self):
        filename = os.path.join(os.path.dirname(__file__), 'data', 'name-1840.jpg')
        expected_text = 'https://efpa.cz/a/7601'
        filter_size = 3

        recognized = decode_qr(filename, filter_size)
        self.assertEqual(expected_text, recognized)


if __name__ == '__main__':
    unittest.main()
