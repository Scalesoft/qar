import unittest
import os

from qar.encode import encode
from qar.decode import decode_qr, decode_bar


class BasicTests(unittest.TestCase):

    def setUp(self):
        self.TMP_FILE_PFX = os.path.join(os.path.dirname(__file__), 'out')
        self.TMP_FILE = os.path.join(os.path.dirname(__file__), self.TMP_FILE_PFX + '.png')

    def tearDown(self):
        if os.path.exists(self.TMP_FILE):
            os.remove(self.TMP_FILE)

    def test_generate_read_qrcode(self):
        input_text = 'testing text'
        encode(input_text, outfile=self.TMP_FILE_PFX, bar=False)
        self.assertTrue(os.path.exists(self.TMP_FILE))

        recognized = decode_qr(self.TMP_FILE, 0)
        self.assertEqual(input_text, recognized)

    def test_generate_read_barcode(self):
        input_text = '1234'
        encode(input_text, outfile=self.TMP_FILE_PFX, bar=True)
        self.assertTrue(os.path.exists(self.TMP_FILE))

        recognized = decode_bar(self.TMP_FILE)
        self.assertEqual(input_text, recognized[:-1])  # TODO: Repaire added last character!!!


if __name__ == '__main__':
    unittest.main()
