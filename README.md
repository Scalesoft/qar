# QaR
Simple QR code and barcode generator and reader.

## Install
Install zbar library dependencies (tested on Ubuntu 16.04).
~~~bash
sudo apt-get install libzbar0 libzbar-dev
~~~

Install qar console app as Python 3 package. In folder where setup.py file is run:
~~~bash
sudo pip3 install .
~~~

## Usage
Generate QR code:
~~~bash    
qar 'Content to encode' -o my_qrcode
~~~

Generate barcode:
~~~bash    
qar 456 -o my_barcode -b
~~~

Read QR code:
~~~bash    
qar -r input_image.png
~~~

Read QR code with filter size 3. It helps to recognize noisy images:
~~~bash
qar -r input_image.png -f 3
~~~

Read barcode:
~~~bash    
qar -r input_image.png -b
~~~
