from setuptools import setup, find_packages

setup(
    name='qar',
    description='Simple QR code and barcode too.',
    version='1.1.0',
    packages=find_packages(),
    install_requires=[
        'pyBarcode==0.7',
        'zbarlight==1.2',
        'qrcode==5.3'
    ],
    entry_points='''
        [console_scripts]
        qar=qar.cli:qar_cmd
    ''',
)
