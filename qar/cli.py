from qar.decode import decode_bar, decode_qr
from qar.encode import encode

import argparse


def qar_cmd():
    parser = argparse.ArgumentParser(description='QR and bar code generator/reader.')
    parser.add_argument('input', help='input image')
    parser.add_argument('--read', '-r', help='Use read mode instead of generator.', action='store_true')
    parser.add_argument('--filter-size', '-f', help='Filter size (QR read only).', type=int, default=0)
    parser.add_argument('--output', '-o', help='Output file name (without extension - .png always). Only for '
                                            'generator mode.', default='out')
    parser.add_argument('--bar', '-b', help='Use barcode instead of QR code.', action='store_true')

    args = parser.parse_args()

    if args.read:
        if args.bar:
            decode_bar(args.input)
        else:
            decode_qr(args.input, args.filter_size)
    else:
        encode(args.input, args.output, args.bar)
