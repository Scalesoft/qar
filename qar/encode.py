import barcode
from barcode.writer import ImageWriter
import qrcode


def encode(input: str, outfile: str, bar: bool):
    if bar:
        _encode_bar(input, outfile)
    else:
        _encode_qr(input, outfile)


def _encode_bar(num: str, output: str):
    ean = barcode.get('code39', num, writer=ImageWriter())
    ean.save(output)


def _encode_qr(msg: str, output: str):
    img = qrcode.make(msg)
    img.save(output + '.png')
