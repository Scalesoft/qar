from PIL import Image, ImageFilter
import zbarlight


def decode_qr(filename: str, filter_size: int) -> str:
    with open(filename, 'rb') as image_file:
        image = Image.open(image_file)
        image.load()
        if filter_size > 0:
            image = image.filter(ImageFilter.MedianFilter(filter_size))
        codes = zbarlight.scan_codes('qrcode', image)
    if codes is not None:
        codes = str(codes[0], 'utf-8')
        print(codes)
    return codes


def decode_bar(filename: str) -> str:
    with open(filename, 'rb') as image_file:
        import zbarlight
        image = Image.open(image_file)
        image.load()
        codes = zbarlight.scan_codes('code39', image)
    if codes is not None:
        codes = str(codes[0], 'utf-8')
        print(codes)
    return codes
